<?php
if(isset($_SERVER['HTTP_REFERER'])) {
    $back = '<p><a href="' . $_SERVER['HTTP_REFERER'] . '" class="back">Powrót</a></p>';
} else {
    $back = null;
}

$lang['manage'][0] = 'Podaj poprawną nazwę.'.$back;
$lang['manage'][1] = 'Podaj poprawnie id.';
$lang['manage'][3] = 'Podany tytuł już istnieje'.$back;
$lang['manage'][4] = 'Taki plik istnieje. Czy wykasować?'.$back;
$lang['manage'][5] = 'Pomyslnie utworzono job'.$back;
$lang['manage'][6] = 'Musisz wpisać zawartość pola tekstowego';
$lang['manage'][7] = 'Taki plik nie istnieje'.$back;
$lang['manage'][9] = 'Nie znaleziono pliku.'.$back;