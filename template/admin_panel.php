<?php
if(!$session -> exists('id') || !$session -> exists('login'))
{
    header('Location: ?page=login', true, 302);
   die();
 return false;
}
?>

<div class="container">

    <div class="row">
        <div class="col-2">
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
              <a class="nav-link" id="v-pills-addjob-tab" data-toggle="pill" href="#addjob" role="tab" aria-controls="addjob" aria-selected="false">Dodaj nową pracę</a>
              <a class="nav-link active" id="v-pills-managejob-tab" data-toggle="pill" href="#managejob" role="tab" aria-controls="managejob" aria-selected="true">Zarządzaj istniejącymi</a>
                <p><a class="nav-link" href="?page=logout">Wyloguj</a></p>    
            </div>
        </div>
        <div class="col-10">
          <div class="tab-content" id="v-pills-tabContent" style="line-height: normal; height: auto; overflow: initial; overflow-x: initial">
              <div class="tab-pane fade" id="addjob" role="tabpanel" aria-labelledby="v-pills-addjob-tab">
                
                  
                  <form action="?page=<?php echo $_GET['page']; ?>" name="addjob-form" method="post" id="addjob-form">
                            <table class="addjob-form">
                                <tbody>
                                    <tr>
                                        <td>
                                            Dodaj nową pracę
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>Nazwa</span> <input type="text" name="title" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <textarea name="text" id="addjob-text"></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button type="submit" form="addjob-form" value="addnewjob-withouttoken" name="submit">Dodaj</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                  
                  
              </div>
              <div class="tab-pane fade show active" id="managejob" role="tabpanel" aria-labelledby="v-pills-managejob-tab">
              <?php if(isset($_GET['edit'])) {
                        $jobData = $manage->getJobData($_GET['edit']);
                        echo "
                            <form action=\"?page=".$_GET['page']."\" name=\"updatejob-form\" method=\"post\" id=\"updatejob-form\">
                            <table class=\"addjob-form\">
                                <tbody>
                                    <tr>
                                        <td>
                                            <a href=\"?page=".$_GET['page']."\"><img src=\"img/back-arrow.png\" class=\"back-ico\"></a> Aktualizacja danych <input type=\"text\" name=\"id\" value=\"".$jobData['id']."\" style=\"display:none\">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>Nazwa</span> <input type=\"text\" name=\"title\" value=\"".$jobData['title']."\" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <textarea name=\"text\" id=\"updatejob-text\">"; include_once($jobData['desc_file']); echo "</textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button type=\"submit\" form=\"updatejob-form\" value=\"updatejob-withouttoken\" name=\"submit\">Aktualizuj</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                        ";
                    } else {
                    $i=1;
                    echo "
                    <table class=\"table table-hover\">
                      <thead>
                        <tr>
                          <th scope=\"col\">#</th>
                          <th scope=\"col\">Nazwa</th>
                          <th scope=\"col\">Akcje</th>
                        </tr>
                      </thead>
                      <tbody>";
                            foreach($jobList as $job){ 
                                echo "<tr> <th scope=\"row\">".$i++."</th>";
                                echo "<td>".$job['title']."</td>";
                                echo "<td><a href=\"?page=".$_GET['page']."&edit=".$job['id']."\">Edytuj</a></td>";
                                echo "<td><form action=\"?page=".$_GET['page']."&delete=".$job['id']."\" name=\"deletejob".$job['id']."-form\" method=\"post\" id=\"deletejob".$job['id']."-form\" onsubmit=\"return confirm('Czy napewno chcesz usunąć?');\">
                                    <button type=\"submit\" form=\"deletejob".$job['id']."-form\" name=\"submit\" value=\"deletejob-withouttoken\">Usuń</button>
                                </form></td></tr>";
                            }
                echo "</tbody>
                    </table>
                    ";
                    }
                ?>
              </div>
            </div>
        </div>
    </div>
    
    
    
    
</div>
