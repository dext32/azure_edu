<div id="push"></div>
  </div>
    <footer>
		<div class="container">
			<div class="footer-info mt-2">
				<nav class="navbar navbar-expand-lg">
                  <ul class="navbar-nav mr-auto">
                      <li class="nav-item">
                          <div>
                            <button class="popper location" data-toggle="popover"></button> 
                              <div class="popper-content" style="display:none">
                                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2445.2818233038083!2d20.927646815962042!3d52.20192586750463!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x471934b19d3ca879%3A0xa234448e3bf7ab97!2sCBSG+Polska!5e0!3m2!1spl!2spl!4v1515104251394" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                          </div>
                      </li>
                      <li class="nav-item">
                        <address>
                            <strong>CBSG Polska</strong><br>
                            ul. Czereśniowa 98<br>
                            02-456 Warszawa<br>
                            tel. +48 22 270 61 62<br>
                        </address>
                      </li>
                  </ul>
                  <ul class="navbar-nav">
                      <li class="nav-item">
                        <div>Copyright © 2018 CBSG Polska sp. z o.o. All rights reserved.</div>
                      </li>
                  </ul>
                </nav>
			</div>
		</div>
	</footer>
  
    <script src="js/jquery.js"></script>
    <script src="js/tinymce/tinymce.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
        tinymce.init({
            selector: '#addjob-text',
            height : 350,
            font_formats: 'Segoe UI Condensed,Segoe UI,Segoe WP,Tahoma,Arial,sans-serif',
            language: 'pl',
            language_url: 'js/tinymce/langs/pl.js',
            plugins: 'code print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
            toolbar1: 'code | formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat'
          });
        tinymce.init({
            selector: '#updatejob-text',
            height : 350,
            font_formats: 'Segoe UI Condensed,Segoe UI,Segoe WP,Tahoma,Arial,sans-serif',
            language: 'pl',
            language_url: 'js/tinymce/langs/pl.js',
            plugins: 'code print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
            toolbar1: 'code | formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat'
          });
        $(document).ready(function(){
            $("#email-tab").click(function(){
                $("#read-description").animate({
                    top: '13vh',
                }, 1500);
            });
            $(".desc-activator").click(function(){
                $("#read-description").animate({
                    top: '13vh'
                }, 1500);
                $("#email-tab").attr("aria-selected", "false");   
                $("#email-tab").removeClass("active show");
            });
            $("#read-description-back-button").click(function(){
                $("#read-description").animate({
                    top: '-100%',
                }, 1500);
                $("#email-tab").attr("aria-selected", "false");   
                $("#email-tab").removeClass("active show");
            });
        });
        $('.popper').popover({
            placement: 'top',
            container: 'body',
            html: true,
            content: function () {
                return $(this).next('.popper-content').html();
            }
        });
    </script>
 </body>
</html>