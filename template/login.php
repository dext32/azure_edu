<div class="container">
    <div class="row justify-content-center">
        <div class="col-4 bg-light py-3 px-3 mt-2">
            <form action="?page=login_action" method="POST">
              <div class="form-group">
                <label for="name">Login</label>
                <input type="text" class="form-control" id="name" name="login" placeholder="Wpisz login">
              </div>
              <div class="form-group">
                <label for="password">Hasło</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Hasło">
              </div>
              <button type="submit" class="btn btn-primary">Log In</button>
            </form>
        </div>
    </div>
</div>

