<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

if(isset($_GET['page'])) {
    $page = strip_tags($_GET['page']);
} else {
    $page = null;
}

switch($page)
{
    case 'main':
        $jobList = $manage->listJobs();
        require 'main.php';
    break;
 
    case 'register':
        require 'register.php';
    break;

    case 'login':
        require 'login.php';
    break;

    case 'admin':
        $jobList = $manage->listJobs();
        if(isset($_POST['submit'])){
            if($_POST['submit'] == 'addnewjob-withouttoken'){
                $manage -> setNewJob($_POST['title'],$_POST['text'],'0');
                header("Refresh:0;URL=?page=".$_GET['page']);
            } elseif ($_POST['submit'] == 'updatejob-withouttoken'){
                $manage -> updateJobData($_POST['id'],$_POST['title'],$_POST['text']);
                header("Refresh:0;URL=?page=".$_GET['page']);
            } elseif ($_POST['submit'] == 'deletejob-withouttoken'){
                $manage -> deleteJob($_GET['delete']);
                header("Refresh:0;URL=?page=".$_GET['page']);
            }
        }
        require 'admin_panel.php';
    break;

        /*
    case 'register_action':
    {
        $register = new Auth($pdo, $session);
        $result = $register -> register(['login' => $_POST['login'], 'password' => $_POST['password'], 'email' => $_POST['email']]);
        echo (is_array($result) && $result[1] == true) ? $result[0] : $result;
        header("Refresh:0;URL=?page=admin");
    }
    break;
    //     */
        
    case 'login_action':
    {
        $login = new Auth($pdo, $session);
        $result = $login -> login(['login' => $_POST['login'], 'password' => $_POST['password']]);

        if(is_array($result) && $result[1] == true)
        {
            echo $result[0];
            header("Refresh:0;URL=?page=admin");
        } else {
            echo $result;
        }
    }
    break;
 
    case 'logout':
    {
        $login = new Auth($pdo);
        $login -> logout();
    }
    break;

    case 'mail':
    {
        if(isset($_POST['submit'])){
            $manage -> PMSendQuery($_POST['topic'],$_POST['message'],$_POST['from']);
            header("Refresh:0;URL=index.php");
        }
    }
        
    default:
        $jobList = $manage->listJobs();
        require 'main.php';
    break;
}

?>