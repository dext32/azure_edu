<ul class='nav circle-container' id="myTab" role="tablist">
    <?php foreach($jobList as $job){
    echo "
    <li class=\"desc-activator\">
        <div id=\"job-".$job['id']."-tab\" data-toggle=\"tab\" href=\"#job-".$job['id']."\" role=\"tab\" aria-controls=\"job-".$job['id']."\" aria-selected=\"false\">
            <span>".$job['title']."</span>
        </div>
    </li>";
    } ?>
    <li class='circle-container-logo'><div><img src='img/Microsoft-Azure_logo.png'></div></li>
</ul>

<div id="read-description">
    <div class="container">
        <div class="row">
            <div class="col-1" style="padding: 0px 1.25vw">
                <div id="read-description-back-button"><img src="img/back-arrow.png" class="back-ico"></div>
            </div>
            <div class="col-11" style="padding-left: 0px; margin-top: 1vh">
                <div class="tab-content" id="myTabContent">
                    <?php foreach($jobList as $job){
                    echo "<div class=\"tab-pane fade\" id=\"job-".$job['id']."\" role=\"tabpanel\" aria-labelledby=\"job-".$job['id']."\">"; include_once($job['desc_file']); echo "</div>".PHP_EOL;
                    } ?>
                    <div class="tab-pane fade" id="email" role="tabpanel" aria-labelledby="email-tab">
                        <form action="?page=mail" method="post" id="email-form">
                            <table class="email-form">
                                <tbody>
                                    <tr>
                                        <td>
                                            Kontakt
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>Od</span> <input type="email" name="from">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="text" name="topic" placeholder="Dodaj temat">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <textarea name="message"></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button type="submit" form="email-form" value="send" name="submit">Wyślij</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        
