﻿<?php

/**
 * Class user
 */
class Manage
{
    /**
     * @var
     */
    private $pdo;
    /**
     * @var array
     */
    private $info = [];
    
    /**
     * @param $pdo
     */
    public function __construct($pdo)
    {
        if (is_object($pdo)) {
            $this->pdo = $pdo;
        }
        
        $langPath = 'text/manage.lang.php';
        if (file_exists($langPath)) {
            $lang = [];
            require_once $langPath;
            $this->info = $lang['manage'];
        }
    }
    
    private static function getFileNameFromString($title){
        if(!is_string($title)){ return $this->info[0]; }
		else {
			return "jobs/".str_replace(" ","-",$title).".html";
		}
    }
    
    public function getJobData($id){
        if(!is_numeric($id)){ return $this->info[1]; } //Podaj id joba
        else {
            $stmt = $this->pdo->prepare('SELECT * FROM `jobs` WHERE id = :jobId');
            $stmt->bindParam(':jobId', $id, PDO::PARAM_INT);
            $stmt->execute();
            
            return $stmt->fetch(PDO::FETCH_ASSOC);
        }
    }
    
	public function setNewJob($title,$content,$certid){
		$filename = self::getFileNameFromString($title);
		$isExistTitle = $this->pdo->prepare('SELECT `title` FROM `jobs` WHERE `title`=:title');
        $isExistTitle->bindParam(':title', $title, PDO::PARAM_STR);
		$isExistTitle->execute();
		if( $isExistTitle->rowCount() > 0){ return array('file_exist' => null, 'info' => $this->info[3]); } //Tytuł już istnieje
		elseif (file_exists($filename)) { return array('file_exist' => true, 'info' => $this->info[4]); } //Taki plik istnieje. Czy wykasować?
			else {
				$queryNewJob = $this->pdo->prepare('INSERT INTO `jobs` (title,desc_file,cert) VALUES (:title,:desc_file,:cert)');
				$queryNewJob->bindParam(':title', $title, PDO::PARAM_STR);
				$queryNewJob->bindParam(':desc_file', $filename, PDO::PARAM_STR);
				$queryNewJob->bindParam(':cert', $certid, PDO::PARAM_INT);
				$queryNewJob->execute();
				$myfile = fopen($filename, "w") or die($this->info[9]); //Nie znaleziono pliku.
				fwrite($myfile, $content);
				fclose($myfile);
				
				return array('file_exist' => false, 'info' => $this->info[5]); //Pomyslnie utworzono job
			}
	}
    
    public function updateJobData($id,$title,$content,$certid = null){
        $jobData = $this->getJobData($id);
        $filename = self::getFileNameFromString($title);
        if( !is_numeric($id) ) return false;
        if( empty($content) ) return $this->info[6]; //Musisz wpisać zawartość pola tekstowego
        try {
            if( !is_null($certid) ) { 
                $queryUpdateJob = $this->pdo->prepare("UPDATE `jobs` SET title=:title, cert=:certid, desc_file=:newfile WHERE id=:jobId");
                $queryUpdateJob->bindParam(':certid', $certid, PDO::PARAM_INT);
            } else {
                $queryUpdateJob = $this->pdo->prepare("UPDATE `jobs` SET title=:title, desc_file=:newfile WHERE id=:jobId");
            }
            $queryUpdateJob->bindParam(':title', $title, PDO::PARAM_STR);
            $queryUpdateJob->bindParam(':newfile', $filename, PDO::PARAM_STR);
            $queryUpdateJob->bindParam(':jobId', $id, PDO::PARAM_INT);
            $queryUpdateJob->execute();
            
            rename($jobData['desc_file'],$filename);
            $myfile = fopen($filename, "w") or die($this->info[9]); //Nie znaleziono pliku.
            fwrite($myfile, $content);
            fclose($myfile);
            return true; //Pomyślnie zmodyfikowano.
        } catch ( PDOException $e ){
            echo $queryUpdateJob . "<br>" . $e->getMessage();
        }
    }

    public function deleteJob($id){
        $jobData = $this->getJobData($id);
        if( !is_numeric($id) ) return false;
        try {
            $queryDeleteJob = $this->pdo->prepare('DELETE FROM `jobs` WHERE id=:jobId');
            $queryDeleteJob->bindParam(':jobId', $id, PDO::PARAM_INT);
            $queryDeleteJob->execute();
            
            if(!file_exists($jobData['desc_file'])) {
              die($this->info[7]); //Taki plik nie istnieje
            } else {
              unlink($jobData['desc_file']);
              return true; //Pomyslnie usunieto zawartość.
            }
        } catch ( PDOException $e ){
            echo $queryDeleteJob . "<br>" . $e->getMessage();
        }
    }
    
    public function listJobs(){
        $queryListJobs = $this->pdo->prepare('SELECT * FROM `jobs`');
        $queryListJobs->execute();
        return $queryListJobs->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function countJobs(){
        return count($this->listJobs());
    }
    
    public function PMSendQuery($subject,$message,$to){
        require 'PHPMailer\PHPMailerAutoload.php';

        require_once('PHPMailer\class.phpmailer.php');
        require_once('PHPMailer\class.smtp.php');
        $mail = new PHPMailer();
        $mail->From = core::$config['mail']['from'];
        $mail->FromName = core::$config['mail']['fromName'];
        $mail->Host = core::$config['mail']['host'];
        $mail->Mailer = core::$config['mail']['mailer'];
        $mail->SMTPAuth = core::$config['mail']['SMTPAuth'];
        $mail->Username = core::$config['mail']['username'];
        $mail->Password = core::$config['mail']['password'];
        $mail->Port = core::$config['mail']['port'];
        $mail->CharSet = 'UTF-8';
        $mail->Subject = $subject;
        $mail->Body = $to.", napisał ze strony ".core::$config['mail']['fromName'].": <br>".$message;
        $mail->SMTPAutoTLS = true;
        $mail->SMTPSecure = 'tls';
        //$mail->AddAddress ($to,$name." ".$surname);
        $mail->AddAddress (core::$config['mail']['from'],$to);

        $mail->IsHTML(true);
        if($mail->Send()){                      
            echo '';
        } else {
            echo 'E-mail nie mógł zostać wysłany';
        }

    }
}

?>